# Date View

# What Is This?
-------------

This is a simple Python application that displays the desired date based on today's date to various calendars

# Requirements
-------------

* Python 3.10
* requests
* beautifulsoup4
* colorama

> This code makes use of the match case (https://docs.python.org/3/whatsnew/3.10.html#other-key-features).
> This Key feature was introduced in Python 3.10

# Sample Execution & Output

First, we run the Python application

```
 python dateview.py
```

The following message will be displayed to the user for get input

```
Please enter {far day number} {calendar}
```

If you want today's date in Gregorian calendar, enter the following command (case insensitive)

```
0 ad
```

Sample output

```
Calendar | Year | Month | Day
AD : 2022 - 7 - 3
------------------------------
```

If you want today's date in Solar calendar, enter the following command (case insensitive)

```
0 solar
```

Sample output

```
Calendar | Year | Month | Day
Solar : 1401 / 4 / 12
------------------------------
```

If you want today's date in Lunar calendar, enter the following command (case insensitive)

```
0 lunar
```

Sample output

```
Calendar | Year | Month | Day
Lunar : 1443 / 12 / 3
------------------------------
```

If you want today's date in all calendars, enter the following command (case insensitive)

```
0 all
```

Sample output

```
Calendar | Year | Month | Day
AD : 2022 - 7 - 3
Solar : 1401 / 4 / 12
Lunar : 1443 / 12 / 3
------------------------------
```

If you want a date 50 days from now in all calendars, enter the following command (case insensitive)

```
+50 all
```

Sample output

```
Calendar | Year | Month | Day
AD : 2022 - 8 - 22
Solar : 1401 / 5 / 31
Lunar : 1444 / 1 / 24
------------------------------
```

If you want the date of the previous 50 days in all calendars, enter the following command (case insensitive)

```
-50 all
```

Sample output

```
Calendar | Year | Month | Day
AD : 2022 - 5 - 14
Solar : 1401 / 2 / 24
Lunar : 1443 / 10 / 12
------------------------------
```