# Import libraries
import requests
from bs4 import BeautifulSoup
from colorama import Fore, init


class DateView:
    def __get_today_s_date(self, date_type='AD'):
        # The designated codes of the html file classes for each type of date
        dates_with_code = {'Solar': 11, 'AD': 22, 'Lunar': 33}
        # The url address to send the request
        url = 'https://www.bahesab.ir/time/'
        # Determining the header to run the code on any operating system
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/50.0.2661.102 Safari/537.36'}

        if date_type not in dates_with_code.keys():
            raise Exception('Date type is invalid')

        # Send an HTTP request to a URL with the GET method
        content = requests.get(url=url, headers=headers)
        # Parse the html content
        soup = BeautifulSoup(content.text, 'html5lib')

        result = soup.find('div', attrs={'class': f'date-{dates_with_code[date_type]}'}).text

        return result

    def __get_date(self, specified_day: int, months_and_days: dict, split_form: str, leap_year: int, leap_month: int,
                   leap_day: int, date_type: str):
        number_of_months = len(months_and_days.keys())
        today = self.__get_today_s_date(date_type=date_type)

        if not isinstance(specified_day, int):
            raise Exception('Specified day is invalid')

        day, month, year = today.split(split_form)
        year = int(year)
        month = int(month)
        day = int(day)
        day += specified_day

        # Specify the desired day
        flag = True
        while flag:
            flag = False
            # Leap year
            if ((year + 1) % leap_year == 0 and date_type != 'AD') or (year % leap_year == 0 and date_type == 'AD'):
                months_and_days[leap_month] = leap_day
            else:
                months_and_days[leap_month] = leap_day - 1

            if day > months_and_days[month]:
                flag = True
                day -= months_and_days[month]
                month += 1
            elif day < 0:
                flag = True
                month = (month - 1 + number_of_months) % number_of_months
                if month == 0:
                    month = number_of_months
                    year -= 1
                day += months_and_days[month]

            if month > number_of_months:
                flag = True
                year += 1
                month -= number_of_months

        result = f'{year} {split_form} {month} {split_form} {day}'

        return result

    def get_date_to_ad(self, specified_day=0):
        # The number of Gregorian months and their number of days
        months_and_days = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}
        split_form = '-'
        leap_year = 4
        leap_month = 2
        leap_day = 29

        result = self.__get_date(specified_day, months_and_days, split_form, leap_year, leap_month, leap_day, 'AD')
        return result

    def get_date_to_solar(self, specified_day=0):
        # The number of Gregorian months and their number of days
        months_and_days = {1: 31, 2: 31, 3: 31, 4: 31, 5: 31, 6: 31, 7: 30, 8: 30, 9: 30, 10: 30, 11: 30, 12: 29}
        split_form = '/'
        leap_year = 4
        leap_month = 12
        leap_day = 30

        result = self.__get_date(specified_day, months_and_days, split_form, leap_year, leap_month, leap_day, 'Solar')
        return result

    def get_date_to_lunar(self, specified_day=0):
        # The number of Gregorian months and their number of days
        months_and_days = {1: 30, 2: 29, 3: 30, 4: 29, 5: 30, 6: 29, 7: 30, 8: 29, 9: 30, 10: 29, 11: 30, 12: 29}
        split_form = '/'
        leap_year = 3
        leap_month = 12
        leap_day = 30

        result = self.__get_date(specified_day, months_and_days, split_form, leap_year, leap_month, leap_day, 'Lunar')
        return result


def main():
    result = ''
    date_view = DateView()

    try:
        specified_day, date_type = input('Please enter {far day number} {calendar}\n').split(' ')
        specified_day = int(specified_day)
    except:
        raise Exception('Invalid input')

    match date_type.lower():
        case 'all':
            result += Fore.BLUE + f'AD : {date_view.get_date_to_ad(specified_day)}\n'
            result += Fore.YELLOW + f'Solar : {date_view.get_date_to_solar(specified_day)}\n'
            result += Fore.CYAN + f'Lunar : {date_view.get_date_to_lunar(specified_day)}'
        case 'ad':
            result = Fore.BLUE + f'AD : {date_view.get_date_to_ad(specified_day)}'
        case 'solar':
            result = Fore.YELLOW + f'Solar : {date_view.get_date_to_solar(specified_day)}'
        case 'lunar':
            result += Fore.CYAN + f'Lunar : {date_view.get_date_to_lunar(specified_day)}'

        # If an exact match is not confirmed, this last case will be used if provided
        case _:
            raise Exception('Invalid command')

    print(Fore.LIGHTWHITE_EX + 'Calendar | Year | Month | Day')
    print(result)
    print(Fore.LIGHTWHITE_EX + 30 * '-')


# Only when this is the original executable file is the condition true
if __name__ == '__main__':
    init(True)
    try:
        main()
    except Exception as e:
        print(Fore.RED + f'{e}')
