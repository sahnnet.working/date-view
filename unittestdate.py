# Import libraries
import unittest

from dateview import DateView


class DateViewTestCase(unittest.TestCase):
    date_view = DateView()

    def test_get_date_to_ad(self):
        test_cases = {
            # Today
            1: (0, '2022 - 7 - 2'),
            # Fixed month
            2: (1, '2022 - 7 - 3'),
            3: (-1, '2022 - 7 - 1'),
            4: (10, '2022 - 7 - 12'),
            # Fixed year
            5: (-5, '2022 - 6 - 27'),
            6: (40, '2022 - 8 - 11'),
            # Without limitation
            7: (-365, '2021 - 7 - 2'),
            8: (365, '2023 - 7 - 2'),
        }

        for k, v in test_cases.items():
            self.assertEqual(self.date_view.get_date_to_ad(v[0]), v[1], msg=f'Test {k}')

        # Test invalid input
        with self.assertRaises(Exception, msg='Test : Invalid input'):
            self.date_view.get_date_to_ad('string')

    def test_get_date_to_solar(self):
        test_cases = {
            # Today
            1: (0, '1401 / 4 / 11'),
            # Fixed month
            2: (1, '1401 / 4 / 12'),
            3: (-1, '1401 / 4 / 10'),
            4: (10, '1401 / 4 / 21'),
            # Fixed year
            5: (-15, '1401 / 3 / 27'),
            6: (30, '1401 / 5 / 10'),
            # Without limitation
            7: (-365, '1400 / 4 / 11'),
            8: (365, '1402 / 4 / 11'),
        }

        for k, v in test_cases.items():
            self.assertEqual(self.date_view.get_date_to_solar(v[0]), v[1], msg=f'Test {k}')

        # Test invalid input
        with self.assertRaises(Exception, msg='Test : Invalid input'):
            self.date_view.get_date_to_solar('string')

    def test_get_date_to_lunar(self):
        test_cases = {
            # Today
            1: (0, '1443 / 12 / 2'),
            # Fixed month
            2: (1, '1443 / 12 / 3'),
            3: (-1, '1443 / 12 / 1'),
            4: (10, '1443 / 12 / 12'),
            # Fixed year
            5: (-15, '1443 / 11 / 17'),
            6: (-60, '1443 / 10 / 1'),
            # Without limitation
            7: (-365, '1442 / 11 / 21'),
            8: (365, '1444 / 12 / 13'),
        }

        for k, v in test_cases.items():
            self.assertEqual(self.date_view.get_date_to_lunar(v[0]), v[1], msg=f'Test {k}')

        # Test invalid input
        with self.assertRaises(Exception, msg='Test : Invalid input'):
            self.date_view.get_date_to_lunar('string')


# Only when this is the original executable file is the condition true
if __name__ == '__main__':
    unittest.main()
